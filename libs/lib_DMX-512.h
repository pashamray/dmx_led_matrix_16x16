/*
 * DMX-512 for AVR
 *
 * lib_DMX-512.h
 *
 */

#ifndef LIB_DMX_512_H_
#define LIB_DMX_512_H_ 1

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define DMX_PORT		PORTD
#define DMX_PIN    		PIND
#define DMX_CPIN   		PD2

#define DMX_CHANNELS 	3
#define IBG   		 	(10)						//interbyte gap [us]

void DMX_Init(void);
uint16_t DMX_GetAddress(void);
void DMX_SetAddress(uint16_t);

uint8_t DMX_GetField(uint8_t);
void DMX_SetField(uint8_t, uint8_t);

void DMX_setTX(void);
void DMX_setRX(void);

#endif
