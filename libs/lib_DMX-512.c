/*
 * DMX-512 for AVR
 *
 * lib_DMX-512.c
 *
 */

#include "lib_DMX-512.h"

volatile uint8_t	DmxTxField[DMX_CHANNELS];	//array of DMX vals (raw)
volatile uint8_t	DmxRxField[DMX_CHANNELS];	//array of DMX vals (raw)
volatile uint16_t	DmxAddress;					//start address
volatile uint16_t	DmxSize;					//size of DMX universe 

volatile uint16_t	gCurDmxCh; //current DMX channel

uint8_t				gDmxState;
uint8_t				*gDmxPnt;
uint16_t			DmxCount;

enum
{ // DMX RX states
	RX_IDLE, RX_BREAK, RX_STARTB, RX_STARTADR, RX_DATA, RX_ERR
};

/*
enum
{ // DMX TX states
	TX_BREAK, TX_STARTB, TX_DATA
};
*/
/*
#define TX_BREAK	(0xFFFF)
#define TX_STARTB	(0)
#define TX_DATA		(1)
*/
void DMX_setTX(void) {
	DMX_PORT |=  (1 << DMX_CPIN);
	/* Enable transmitter */
	UCSRB = (1 << TXEN) | (1 << TXCIE);
}

void DMX_setRX(void) {
	DMX_PORT &= ~(1 << DMX_CPIN);
	/* Enable receiver  */
	UCSRB = (1 << RXEN) | (1 << RXCIE);
}

void DMX_Init(void) {

	unsigned char UBRR = (((F_CPU / 16) / 250000) - 1);
	UBRRH = (UBRR >> 8);
	UBRRL = (UBRR); // 250k baud, 8N2
	// USART initialization
	// Communication Parameters: 8 Data, 2 Stop, No Parity
	// USART Receiver: Off
	// USART Transmitter: Off
	// USART Mode: Asynchronous
	// USART Baud Rate: 250000
	UCSRA = 0x00;
	UCSRB = 0x00;
	// Set frame format: 8data, 2stop bit 
	UCSRC = (1 << URSEL) | (1 << USBS) | (3 << UCSZ0);
	UDR = 0; //start USART
	//Data
	gDmxState = 0; //start with break

	uint8_t i;
	for (i = 0; i < sizeof(DmxRxField); i++) {
		DmxRxField[i] = 0;
	}
}

unsigned int DMX_GetAddress() {
	return DmxAddress;
}

void DMX_SetAddress(uint16_t dmx_address) {
	if(dmx_address != DmxAddress) {
		DmxAddress = dmx_address;
	}
}

uint8_t DMX_GetField(uint8_t field) {
	return DmxRxField[field - 1];
}

void DMX_SetField(uint8_t field, uint8_t value) {
	DmxTxField[field - 1] = value;
}

ISR(USART_RXC_vect) {

	uint8_t USARTstate = UCSRA;						//get state
	uint8_t DmxByte    = UDR;						//get data
	uint8_t DmxState   = gDmxState;					//just get once from SRAM!!!
	 
	if (USARTstate &(1<<FE)) {						//check for break
		UCSRA    &= ~(1<<FE);						//reset flag
		gDmxState = RX_BREAK;
		gDmxPnt   = ((uint8_t*) DmxRxField + 2);
		return;
	}

	if (DmxState != RX_ERR) DmxCount++;				//just count DMX bytes

	if (DmxState == RX_BREAK) {
		DmxSize = DmxCount - 1;
		if (DmxByte == 0) {
			gDmxState = RX_STARTB;					//normal start code detected
			//DmxRefresh++;
			DmxCount = 0;							//reset frame counter
		} else if (DmxByte == 0xCC)	{				//RDM sc detected
			//RdmRefresh++;
			gDmxState = RX_ERR;
		} else {
			//ErrRefresh++;							//invalid sc
			gDmxState = RX_ERR;
		}
	} else if (DmxState == RX_STARTB) {
		if (DmxCount == DmxAddress)	{				//start address reached?
			gDmxState     = RX_STARTADR;
			DmxRxField[0] = DmxByte;
		}
	} else if (DmxState == RX_STARTADR) {
		DmxRxField[1] = DmxByte;
		gDmxState     = RX_DATA;
	} else if (DmxState == RX_DATA) {
		uint8_t *DmxPnt;
		DmxPnt  = gDmxPnt;
		*DmxPnt = DmxByte;
		if (++DmxPnt >= (DmxRxField + sizeof(DmxRxField))) {	//all ch received?
			gDmxState = RX_IDLE;
		} else {
			gDmxPnt = DmxPnt;
		}
	}
}

ISR(USART_TXC_vect) {
/*
	unsigned char DmxState = gDmxState;

	if (DmxState == TX_BREAK) {
		UBRRH = 0;
		UBRRL = (unsigned char) (F_CPU / 1600); //90.9kbaud
		UDR = 0; //send break
		gDmxState = TX_STARTB;
	} else if (DmxState == TX_STARTB) {
		UBRRH = 0;
		UBRRL = (unsigned char) (((F_CPU / 16) / 250000) + 1); // 250k baud
		UDR = 0; 							//send start byte
		gDmxState = TX_DATA;
		gCurDmxCh = 0;
	} else {
		_delay_us(IBG);
		unsigned int CurDmxCh = gCurDmxCh;
		UDR = DmxTxField[CurDmxCh++]; //send data
		if (CurDmxCh == sizeof(DmxTxField)) {
			gDmxState = TX_BREAK; //new break if all ch sent
		} else {
			gCurDmxCh = CurDmxCh;
		}
	}
*/
/*
	uint16_t DmxCh = gCurDmxCh;
	if (DmxCh == TX_BRK) {						//start break
		UBRRH  = 0;
		if (Flags &(1<<SLOW_TX)) {
			UBRRL  = 17;
		} else {
			UBRRL  = 9;
			UCSRC  = (1 << URSEL) | (3 << UCSZ0);		//8n1
		}
		UDR = 0;								//send break
		gCurDmxCh = TX_STARTB;
	} else if (DmxCh == TX_STARTB) {
		UBRRH  = 0;
		UBRRL  = (((F_CPU / 16) / 250000) - 1);		//250kbaud
		UCSRC  = (1 << URSEL) | (3 << UCSZ0) | (1 << USBS);
		UDR    = 0;									//send start byte
		gCurDmxCh = TX_DATA;
	} else {
		if (Flags &(1 << SLOW_TX)) {
			_delay_us(20);						//inter byte gap in slow mode
		}
		if (DmxCh & (1 << 0)) { 
			UDR = TxVal;						//send data
		} else {
			UDR = (0xFF - TxVal);
		}
		gCurDmxCh++;
		if (Flags &(1 << SLOW_TX)) {
			if (DmxCh == 511) gCurDmxCh = TX_BRK;
		} else {
			if (DmxCh == 48)  gCurDmxCh = TX_BRK;
		}
	}
*/
}
