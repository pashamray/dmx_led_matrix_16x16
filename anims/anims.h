/*
 * anims.h
 *
 *  Created on: 08.01.2014
 *      Author: xakepok
 */

#ifndef ANIMS_H_
#define ANIMS_H_ 1

#include <avr/pgmspace.h>

//#include "two_colors_shift_g.h"
#include "crosshair.h"
#include "waves.h"
#include "stars.h"
#include "scan_line.h"
//#include "two_scan_line.h"
#include "zoom_circle.h"
#include "zoom_square.h"
#include "rotate_long_line.h"
#include "diagonal_long_line.h"
#include "zoom_squares.h"
#include "wash.h"
#include "snake_rotate.h"
#include "dots_to_center.h"
#include "two_cube_shift.h"

#define SIZE_COLS   16
#define SIZE_ROWS   16

#define COUNT_ANIMS 16

#define size(anim) ((sizeof(anim) / sizeof(prog_uint16_t)) / SIZE_ROWS)

enum {
	FLAG_R = 0,	/* USE COLOR RED */
	FLAG_G = 1,	/* USE COLOR GREEN */
	FLAG_B = 2,	/* USE COLOR BLUE */
	FLAG_W = 3,	/* USE COLOR WHITE */
	FLAG_4,		/* RESERVED */
	FLAG_5,		/* RESERVED */
	FLAG_6,		/* RESERVED */
	FLAG_7,		/* RESERVED */
	FLAG_8,		/* RESERVED */
	FLAG_9,		/* RESERVED */
	FLAG_MV,	/* MIRROR VERTICAL */
	FLAG_MG,	/* MIRROR GORIZONT */
	FLAG_SL,	/* SHIFT LEFT */
	FLAG_SR,	/* SHIFT RIGHT */
	FLAG_SD,	/* SHIFT DOWN */
	FLAG_ST		/* SHIFT TOP */
};

/*
 * animations
 */
 prog_uint16_t * matrix_animations[COUNT_ANIMS] = {
		wash,
		snake_rotate,
		zoom_squares,
		diagonal_long_line,
		diagonal_long_line,
		stars,
		two_cube_shift,
		waves,
		scan_line,
		zoom_circle,
		stars,
		zoom_square,
		two_cube_shift,
		rotate_long_line,
		crosshair,
		dots_to_center
};

/*
 * settings animations
 */
const unsigned int matrix_animSett[COUNT_ANIMS] = {
		/* WASH */					(0x0000),														  				  	// Change color
		/* SNAKE ROTATE */ 			((1 << FLAG_W)  | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),  				  	// RGBW
		/* ZOOM FOUR SQUARES */		((1 << FLAG_W)  | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),  				  	// RGBW
		/* DIAGONAL LONG LINE */	((1 << FLAG_SR) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),	// RGBW & shift to RIGHT
		/* DIAGONAL LONG LINE */	((1 << FLAG_SR) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R) | (1 << FLAG_MG)),	// RGBW & shift to RIGHT & MIRROR GORIZONTAL
		/* STARS */       			((1 << FLAG_ST) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),	// RGBW & shift to TOP
		/* TWO CUBE SHIFT */ 		((1 << FLAG_ST) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),	// RGBW & shift to TOP
		/* WAVES */       			((1 << FLAG_SR) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),	// RGBW & shift to RIGHT
		/* SCAN LINE */				((1 << FLAG_ST) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),	// RGBW & shift to TOP
		/* ZOOM CIRCLE */ 			((1 << FLAG_W)  | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),  				  	// RGBW
		/* STARS */       			((1 << FLAG_SD) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),	// RGBW & shift to DOWN
		/* ZOOM SQUARE */ 			((1 << FLAG_W)  | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),   				// RGBW
		/* TWO CUBE SHIFT */ 		((1 << FLAG_SR) | (1 << FLAG_W) | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),	// RGBW & shift to RIGHT
		/* ROTATE LONG LINE */  	((1 << FLAG_W)  | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),  		  			// RGBW
		/* CROSHAIR */			  	((1 << FLAG_W)  | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R)),  		  			// RGBW
		/* DOTS TO CENTER */		((1 << FLAG_W)  | (1 << FLAG_B) | (1 << FLAG_G) | (1 << FLAG_R))  				  	// RGBW
};

/*
 * size animations
 */
const unsigned char matrix_animSize[COUNT_ANIMS] = {
		size(wash),
		size(snake_rotate),
		size(zoom_squares),
		size(diagonal_long_line),
		size(diagonal_long_line),
		size(stars),
		size(two_cube_shift),
		size(waves),
		size(scan_line),
		size(zoom_circle),
		size(stars),
		size(zoom_square),
		size(two_cube_shift),
		size(rotate_long_line),
		size(crosshair),
		size(dots_to_center)
};


#endif /* ANIMS_H_ */
