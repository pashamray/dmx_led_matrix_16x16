/*
 *        File: lib_MBI5026.c
 *  Created on: 16.05.2012
 *      Author: Xakep.OK
 */

#include "lib_MBI5026.h"

void MBI5026_Init(void)
{
	MBI5026_LATCH_DOWN;
	MBI5026_CLK_DOWN;
	MBI5026_DDR |= (MBI5026_DATA_PIN | MBI5026_CLK_PIN | MBI5026_LATCH_PIN);

	unsigned char i;

	for (i = 0; i < 16; ++i)
	{
		MBI5026_Write(0x0000, 1); // All off
	}

}

void MBI5026_Write(unsigned int data, unsigned char latch)
{
	unsigned char i;

	for (i = 0; i < 16; ++i)
	{
		MBI5026_CLK_DOWN;
		if (data & _BV(i))
		{
			MBI5026_PORT |= MBI5026_DATA_PIN;
		}
		else
		{
			MBI5026_PORT &= (unsigned char) ~MBI5026_DATA_PIN;
		}
		MBI5026_CLK_UP;
	}

	if(latch > 0)
	{
		MBI5026_LATCH_PULSE;
	}
}
