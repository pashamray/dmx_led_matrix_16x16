/*
 * diagonal_long_line.h
 *
 *  Created on: 11.01.2014
 *      Author: xakepok
 */

#ifndef DIAGONAL_LONG_LINE_H_
#define DIAGONAL_LONG_LINE_H_ 1

prog_uint16_t PROGMEM diagonal_long_line[] = {
		0xC000, 0xE000, 0x7000, 0x3800, 0x1C00, 0x0E00, 0x0700, 0x0380, 0x01C0, 0x00E0, 0x0070, 0x0038, 0x001C, 0x000E, 0x0007, 0x0003,
};

#endif /* DIAGONAL_LONG_LINE_H_ */
