/*
 *        File: lib_MATRIX.c
 *  Created on: 16.05.2012
 *      Author: Xakep.OK
 */

#include "lib_MATRIX.h"
#include "lib_MBI5026.h"

#include "../anims/anims.h"

#define bitset(var, bitno) ((var) |= 1 << (bitno))
#define bitclr(var, bitno) ((var) &= ~(1 << (bitno)))

uint16_t matrix_FrameBuffer[16];
uint16_t matrix_tmpFrameBuffer[16];

/** The currently playing animation */
prog_uint16_t * matrix_currentAnimation;
/** The currently color */
volatile uint8_t matrix_animColor;
volatile uint8_t matrix_currentColor;
/** The number of frames in the current animation */
volatile uint8_t matrix_animationFrames;
volatile uint8_t matrix_animationFramesTmp;

volatile uint16_t matrix_animationSetting;
volatile uint8_t  matrix_direction = 0;

void matrix_copyBuffer(void) {
	uint8_t i;
	for(i = 0; i < 16; ++i)	{
		matrix_tmpFrameBuffer[i] = matrix_FrameBuffer[i];
	}
}

void matrix_shiftUp(void) {
	matrix_copyBuffer();
	uint8_t i;
	for(i = 1; i < 16; ++i) {
		matrix_FrameBuffer[i] = matrix_tmpFrameBuffer[i-1];
	}
	matrix_FrameBuffer[0] = matrix_tmpFrameBuffer[15];
}

void matrix_shiftDown(void) {
	matrix_copyBuffer();
	uint8_t i;
	for(i = 1; i < 16; ++i) {
		matrix_FrameBuffer[i-1] = matrix_tmpFrameBuffer[i];
	}
	matrix_FrameBuffer[15] = matrix_tmpFrameBuffer[0];
}

void matrix_shiftRight(void) {
	matrix_copyBuffer();
	uint8_t i;
	for(i = 0; i < 16; ++i) {
		matrix_FrameBuffer[i] = ((matrix_tmpFrameBuffer[i] << 1) | (matrix_tmpFrameBuffer[i] >> 15));
	}
}

void matrix_shiftLeft(void) {
	matrix_copyBuffer();
	uint8_t i;
	for(i = 0; i < 16; ++i) {
		matrix_FrameBuffer[i] = ((matrix_tmpFrameBuffer[i] >> 1) | ((matrix_tmpFrameBuffer[i] & 1) << 15));
	}
}
/*
void matrix_mirror_vertical(void) {

}
*/
void matrix_colorAnim(uint8_t color) {
	matrix_animColor = color;
}

void matrix_toggleDirect(void) {
	matrix_direction = (!matrix_direction);
}

uint8_t matrix_getCountAnims(void) {
	return COUNT_ANIMS;
}

uint8_t matrix_countAnimFrames(uint8_t animation) {
	return matrix_animSize[animation - 1];
}

uint8_t matrix_getCoefficient(void) {
	return (uint8_t)(255 / COUNT_ANIMS);
}

void matrix_playAnimation(uint8_t animation) {
	
	if(animation > COUNT_ANIMS) {
	   animation = COUNT_ANIMS;
	}

    if(animation > 0) {
		animation -= 1;
    }

	matrix_currentAnimation   = matrix_animations[animation];
	matrix_animationFrames    = matrix_animSize[animation];
	matrix_animationFramesTmp = matrix_animSize[animation];
	matrix_animationSetting   = matrix_animSett[animation];

	if(matrix_animationFrames == 1) { // if one frame anim
		matrix_readProgmem(matrix_currentAnimation);
	}

	matrix_Update = matrix_animationCallback;
}

void matrix_animationCallback(void) {

	if (matrix_animationFramesTmp > 1) { // if many frames anim
		if(matrix_direction) {
			if(!matrix_animationFrames) {
				matrix_animationFrames = matrix_animationFramesTmp;
			}
			--matrix_animationFrames;
		} else {
			++matrix_animationFrames;
			if(matrix_animationFrames >= matrix_animationFramesTmp) {
				matrix_animationFrames = 0;
			}
		}
		matrix_readProgmem(matrix_currentAnimation	+ (matrix_animationFrames * 16));
	} else {
		switch((matrix_animationSetting & 0xF000)) {
			case (1 << FLAG_SL):
				if(matrix_direction) {
					matrix_shiftLeft();
				} else {
					matrix_shiftRight();
				}
				break;
			case (1 << FLAG_SR):
				if(matrix_direction) {
					matrix_shiftRight();
				} else {
					matrix_shiftLeft();
				}
				break;
			case (1 << FLAG_SD):
				if(matrix_direction) {
					matrix_shiftDown();
				} else {
					matrix_shiftUp();
				}
				break;
			case (1 << FLAG_ST):
				if(matrix_direction) {
					matrix_shiftUp();
				} else {
					matrix_shiftDown();
				}
				break;
		}
	}

	uint8_t  color = (((matrix_animationSetting & 0x000F) > 0) ? (matrix_animationSetting & 0x000F) : matrix_animColor);
	uint8_t i;
	uint16_t row;

	for (i = 0; i < 16; ++i) {
		row = 0;
		if (color == 0) {
			row = 0x0000;
		} else {
			if (i % 2) {
				if (color & (1 << FLAG_W))	{
					row |= 0x5555;
				}
				if (color & (1 << FLAG_G)) {
					row |= 0xAAAA;
				}
			} else {
				if (color & (1 << FLAG_B)) {
					row |= 0x5555;
				}
				if (color & (1 << FLAG_R)) {
					row |= 0xAAAA;
				}
			}
		}
		matrix_tmpFrameBuffer[i] = (matrix_FrameBuffer[i] & row);
	}

	matrix_Out(matrix_tmpFrameBuffer);
}

void matrix_colorCallback(void)
{
	uint8_t color = matrix_currentColor;
	uint16_t row;
	uint8_t i;
	for (i = 0; i < 16; ++i) {
		row = 0;
		if (color == 0) {
			row = 0x0000;
		} else {
			if (i % 2) {
				if (color & COLOR_WHITE)	{
					row |= 0x5555;
				}
				if (color & COLOR_GREEN) {
					row |= 0xAAAA;
				}
			} else {
				if (color & COLOR_BLUE) {
					row |= 0x5555;
				}
				if (color & COLOR_RED) {
					row |= 0xAAAA;
				}
			}
		}
		matrix_FrameBuffer[i] = row;
	}

	matrix_Out(matrix_FrameBuffer);
}

void matrix_readProgmem(prog_uint16_t * gsArray)
{
	cli();
	prog_uint16_t *gsArrayp = gsArray;
	prog_uint16_t *gsDatap  = matrix_FrameBuffer;

	while (gsDatap < matrix_FrameBuffer + 16) {
		*gsDatap++ = pgm_read_word(gsArrayp++);
	}
	sei();
}

void matrix_Init(unsigned char cols, unsigned char rows)
{
	MBI5026_Init();
}

void matrix_Clear(void) {
	uint8_t i, latch;
	for (i = 0; i < 16; ++i) {
		latch = (i == 15) ? 1 : 0;
		MBI5026_Write(0x0000, latch);
	}
}

/*
 * LedMatrix_Out - Matrix output
 */
void matrix_Out(uint16_t frame[16]) {

	uint8_t  i, latch;
	
	uint8_t j;
	uint16_t tmp_frame[16];

	for(i = 0; i < 16; ++i) {
		tmp_frame[i] = frame[i];
	}

	if ((matrix_animationSetting & 0x0F00) & (1 << FLAG_MV)) {
		for (i = 15; i > 0; --i) {
			tmp_frame[i] = frame[15 - i];
		}
	}

	if ((matrix_animationSetting & 0x0F00) & (1 << FLAG_MG)) {
		for (i = 0; i < 16; ++i) {
			tmp_frame[i] = 0x0000;
			for (j = 15; j > 0; --j) {
				if(frame[i] & (1 << j)) {
					bitset(tmp_frame[i], 15 - j); 
				} else {
					bitclr(tmp_frame[i], 15 - j);
				}
			}
		}
	}

	for (i = 0; i < 16; ++i) {
		latch = (i == 15) ? 1 : 0;
		MBI5026_Write(tmp_frame[i], latch);
	}
}

void matrix_setColor(uint8_t numColor) {
	matrix_currentColor = numColor;
	matrix_Update = matrix_colorCallback;
}
