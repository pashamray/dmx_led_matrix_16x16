/*
 *        File: lib_LEDMatrix16x16.h
 *  Created on: 13.04.2012
 *      Author: Xakep.OK
 */

#ifndef LIB_LEDMATRIX_H_
#define LIB_LEDMATRIX_H_ 1

#include <avr/interrupt.h>
#include <avr/pgmspace.h>

void (*matrix_Update)(void);

void matrix_copyBuffer(void);

void matrix_shiftUp(void);
void matrix_shiftDown(void);
void matrix_shiftRight(void);
void matrix_shiftLeft(void);

void matrix_colorAnim(uint8_t);
void matrix_toggleDirect(void);

unsigned char matrix_getCountAnims(void);
unsigned char matrix_countAnimFrames(uint8_t animation);
unsigned char matrix_getCoefficient(void);

void matrix_animationCallback(void);
void matrix_playAnimation(uint8_t animation);
void matrix_readProgmem(prog_uint16_t * gsArray);

void matrix_setColor(uint8_t);
void matrix_colorCallback(void);

void matrix_Init(uint8_t weight, uint8_t height);
void matrix_Out(uint16_t frame[16]);

#define		COLOR_BLACK 0
#define		COLOR_RED	1
#define		COLOR_GREEN	2
#define		COLOR_BLUE	4
#define		COLOR_WHITE	8

#endif /* LIB_LEDMATRIX_H_ */
