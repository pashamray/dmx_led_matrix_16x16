/*
 * main.c
 *
 *  Created on: 10.02.2012
 *      Author: xakepok
 *
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define		DEBUG_PORT	PORTC
#define		DEBUG_PIN	PINC
#define		DEBUG_CPIN	PC0

#include "libs/lib_DMX-512.h"
#include "libs/lib_MATRIX.h" 

volatile uint16_t g_Flags = 0;
volatile uint16_t g_DipState = 0;
volatile uint16_t g_speed_count = 0, g_sound_color = 0, g_color_count = 0, g_time_count = 0;

inline void init(void);
inline uint16_t get_DipState(void);

enum {
	FLAG_R,
	FLAG_G,
	FLAG_B,
	FLAG_W,

	FLAG_SPEE1,
	FLAG_SPEE2,
	FLAG_SPEE3,

	FLAG_ANIMS,
	FLAG_SOUND,
	FLAG_DMXMO
};

#define START_COLOR COLOR_RED

int main(void) {

	uint16_t g_speedTimer = 1;
	uint8_t  g_animsNum = 0, g_colorNum = START_COLOR;
	uint8_t  m_Color = START_COLOR, m_Anims = 1, m_Speed = 1;

	_delay_ms(500); // wait power stable

	init();
	matrix_Init(16, 16);
	DMX_Init();

	sei();

	while (1) {

		g_DipState = get_DipState();

		if (g_DipState & (1 << FLAG_DMXMO)) {							// manual mode

			DMX_SetAddress(1); 
			DMX_setTX();

			if (!(g_DipState & (1 << FLAG_SOUND))) {					// if sound mode
				g_Flags  &= ~(1 << FLAG_SOUND);							// reset sound global flag
			}

			m_Speed = (((g_DipState & 0x0060) >> 5) * 85);				// speed = dip * 85

			if (g_DipState & (1 << FLAG_ANIMS)) {						// if anims
				m_Anims = (g_DipState & 0x001F);						// anims 0-30
				if(m_Anims == 0x1F) {									// if anim = 31
					m_Anims = g_animsNum;								// anim++
				}
				m_Color = g_colorNum;									// set color cycle
			} else {													// if colors
				m_Anims = 0;
				m_Color = (g_DipState & 0x000F);						// color 0-15
				if(m_Color == 0) {
					m_Color = g_colorNum;								// set color cycle
				}
			}

			if(m_Speed == 0) {
				g_speedTimer = 1500;
			} else if(m_Speed == 85) {
				g_speedTimer = 500;
			} else if(m_Speed == 170) {
				g_speedTimer = 100;
			} else if(m_Speed == 255) {
				g_speedTimer = 25;
			}

			DMX_SetField(1, (m_Color * 17));
			DMX_SetField(2, (m_Anims * matrix_getCoefficient()));
			DMX_SetField(3,  m_Speed);

		} else {														// dmx mode

			DMX_SetAddress((g_DipState & 0x01FF));
			DMX_setRX();

			uint8_t dmx_field = 0;
			dmx_field = DMX_GetField(1);
			if(dmx_field < 15) {
				m_Color = 0;
			} else if((dmx_field >= 15)  && (dmx_field < 30))   {					// colors
				m_Color = 1;
			} else if((dmx_field >= 30)  && (dmx_field < 45))   {
				m_Color = 2;
			} else if((dmx_field >= 45)  && (dmx_field < 60))   {
				m_Color = 3;
			} else if((dmx_field >= 60)  && (dmx_field < 75))   {
				m_Color = 4;
			} else if((dmx_field >= 75)  && (dmx_field < 90))   {
				m_Color = 5;
			} else if((dmx_field >= 90)  && (dmx_field < 105))  {
				m_Color = 6;
			} else if((dmx_field >= 105) && (dmx_field < 120))  {
				m_Color = 7;
			} else if((dmx_field >= 120) && (dmx_field < 135))  {
				m_Color = 8;
			} else if((dmx_field >= 135) && (dmx_field < 150))  {
				m_Color = 9;
			} else if((dmx_field >= 150) && (dmx_field < 165))  {
				m_Color = 10;
			} else if((dmx_field >= 165) && (dmx_field < 180))  {
				m_Color = 11;
			} else if((dmx_field >= 180) && (dmx_field < 195))  {
				m_Color = 12;
			} else if((dmx_field >= 195) && (dmx_field < 210))  {
				m_Color = 13;
			} else if((dmx_field >= 210) && (dmx_field < 225))  {
				m_Color = 14;
			} else if((dmx_field >= 225) && (dmx_field < 240))  {
				m_Color = 15;
			} else if((dmx_field >= 240) && (dmx_field <= 255)) {
				m_Color = g_colorNum;
			}

			dmx_field = DMX_GetField(2);
			m_Anims = (uint8_t)(dmx_field / matrix_getCoefficient());
			/*
			if(dmx_field < 20) {
				m_Anims = 0;
			} else if((dmx_field >= 20)  && (dmx_field < 40))   {					// anims
				m_Anims = 1;
			} else if((dmx_field >= 40)  && (dmx_field < 60))   {
				m_Anims = 2;
			} else if((dmx_field >= 60)  && (dmx_field < 80))   {
				m_Anims = 3;
			} else if((dmx_field >= 80)  && (dmx_field < 100))  {
				m_Anims = 4;
			} else if((dmx_field >= 100) && (dmx_field < 120))  {
				m_Anims = 5;
			} else if((dmx_field >= 120) && (dmx_field < 140))  {
				m_Anims = 6;
			} else if((dmx_field >= 140) && (dmx_field < 160))  {
				m_Anims = 7;
			} else if((dmx_field >= 160) && (dmx_field < 180))  {
				m_Anims = 8;
			} else if((dmx_field >= 180) && (dmx_field < 200))  {
				m_Anims = 9;
			} else if((dmx_field >= 200) && (dmx_field < 220))  {
				m_Anims = 10;
			} else if((dmx_field >= 220) && (dmx_field < 240))  {
				m_Anims = 11;
			} else if((dmx_field >= 240) && (dmx_field <= 255)) {
				m_Anims = 12;
			}
			*/

			dmx_field = DMX_GetField(3);
			if(dmx_field < 15) {
				g_speedTimer = 1500;
			} else if((dmx_field >= 15)  && (dmx_field < 30))   {					// speed
				g_speedTimer = 1400;
			} else if((dmx_field >= 30)  && (dmx_field < 45))   {
				g_speedTimer = 1300;
			} else if((dmx_field >= 45)  && (dmx_field < 60))   {
				g_speedTimer = 1200;
			} else if((dmx_field >= 60)  && (dmx_field < 75))   {
				g_speedTimer = 1100;
			} else if((dmx_field >= 75)  && (dmx_field < 90))   {
				g_speedTimer = 1000;
			} else if((dmx_field >= 90)  && (dmx_field < 105))  {
				g_speedTimer = 900;
			} else if((dmx_field >= 105) && (dmx_field < 120))  {
				g_speedTimer = 800;
			} else if((dmx_field >= 120) && (dmx_field < 135))  {
				g_speedTimer = 700;
			} else if((dmx_field >= 135) && (dmx_field < 150))  {
				g_speedTimer = 600;
			} else if((dmx_field >= 150) && (dmx_field < 165))  {
				g_speedTimer = 500;
			} else if((dmx_field >= 165) && (dmx_field < 180))  {
				g_speedTimer = 40;
			} else if((dmx_field >= 180) && (dmx_field < 195))  {
				g_speedTimer = 300;
			} else if((dmx_field >= 195) && (dmx_field < 210))  {
				g_speedTimer = 200;
			} else if((dmx_field >= 210) && (dmx_field < 225))  {
				g_speedTimer = 100;
			} else if((dmx_field >= 225) && (dmx_field < 240))  {
				g_speedTimer = 50;
			} else if((dmx_field >= 240) && (dmx_field <= 255)) {
				g_speedTimer = 25;
			}
		}

		static uint8_t m_Anims_tmp = 0;
		if(m_Anims > 0) {
			matrix_colorAnim(m_Color);
			if(m_Anims_tmp != m_Anims) {
			   m_Anims_tmp  = m_Anims;
				matrix_playAnimation(m_Anims); // play
			}
		} else {
			m_Anims_tmp = 0;
			matrix_setColor(m_Color);
		}

		// ============================================================================
		// ============================================================================
		// ============================================================================

		if(!g_time_count) {
			g_time_count = 10000;
			g_animsNum += 1;

			if(g_animsNum > matrix_getCountAnims()) {
			   g_animsNum = 1;
			}
		}

		if (!g_sound_color) {
			g_sound_color = 250;

			if((g_Flags &   (1 << FLAG_SOUND))) {
				g_Flags &= ~(1 << FLAG_SOUND); // unset sound flag
				g_colorNum++;
				matrix_toggleDirect();
			}
		}

		if(!g_color_count) {
			g_color_count = 500;
			if(!(g_DipState & (1 << FLAG_SOUND)) && (g_DipState & (1 << FLAG_ANIMS))) {
				g_colorNum++;
			}
		}

		if (!g_speed_count) {				// speed count = 0
			g_speed_count = g_speedTimer;	// speed color = g_speedTimer

			if(!(g_DipState & (1 << FLAG_SOUND)) && !(g_DipState & (1 << FLAG_ANIMS))) {
				g_colorNum++;
			}

			if (matrix_Update) {			// if need animation update
				matrix_Update();			// update animation
			}
		}

		if(g_colorNum > 15) {
			g_colorNum = 1;
		}
	}
	return 0;
}

ISR(TIMER1_OVF_vect) {

	if(g_speed_count) --g_speed_count; // decrement speed count
	if(g_sound_color) --g_sound_color; // decrement sound count
	if(g_color_count) --g_color_count; // decrement color count
	if(g_time_count)  --g_time_count;  // decrement time count

	TCNT1H = (0xFC); // 1000 Hz
	TCNT1L = (0x17);
}

ISR(INT1_vect) {
	g_Flags |= (1 << FLAG_SOUND); // if sound interrupt
}

inline uint16_t get_DipState(void) {

	uint16_t dip_map = 0;

	if (PINB & (1 << PB1))
		dip_map |= (1 << 0);		// R
	if (PINB & (1 << PB2))
		dip_map |= (1 << 1);		// G
	if (PINB & (1 << PB3))
		dip_map |= (1 << 2);		// B
	if (PINB & (1 << PB4))
		dip_map |= (1 << 3);		// W
	if (PINB & (1 << PB5))
		dip_map |= (1 << 4);		// ANIM 
	if (PINB & (1 << PB0))
		dip_map |= (1 << 5);		// SPEED BIT 1
	if (PIND & (1 << PD7))
		dip_map |= (1 << 6);		// SPEED BIT 2
	if (PIND & (1 << PD6))
		dip_map |= (1 << 7);		// ANIMS
	if (PIND & (1 << PD5))
		dip_map |= (1 << 8);		// SOUND
	if (PIND & (1 << PD4))
		dip_map |= (1 << 9);		// DMX MODE

	return ~(dip_map);		// invert state
}

inline void init(void) {
	
	// Declare your local variables here

	// Input/Output Ports initialization
	// Port B initialization
	// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
	// State7=T State6=T State5=P State4=P State3=P State2=P State1=P State0=P
	PORTB = 0x3F;
	DDRB = 0x00;

	// Port C initialization
	// Func6=In Func5=Out Func4=Out Func3=Out Func2=Out Func1=Out Func0=Out
	// State6=T State5=1 State4=1 State3=1 State2=1 State1=1 State0=1
	PORTC = 0x3F;
	DDRC = 0x3F;

	// Port D initialization
	// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=Out Bit1=In Bit0=In 
	DDRD  = (0 << DDD7) | (0 << DDD6) | (0 << DDD5) | (0 << DDD4) | (0 << DDD3) | (1 << DDD2) | (0 << DDD1) | (0 << DDD0);
	// State: Bit7=P Bit6=P Bit5=P Bit4=P Bit3=P Bit2=0 Bit1=T Bit0=T 
	PORTD = (1 << PORTD7) | (1 << PORTD6) | (1 << PORTD5) | (1 << PORTD4) | (1 << PORTD3) | (0 << PORTD2) | (0 << PORTD1) | (1 << PORTD0);

	// Timer/Counter 0 initialization
	// Clock source: System Clock
	// Clock value: 7,813 kHz
	TCCR0 = 0x00;
	TCNT0 = 0x00;

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 1000,000 kHz
	// Mode: Normal top=0xFFFF
	// OC1A output: Disconnected
	// OC1B output: Disconnected
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer Period: 1 us
	// Timer1 Overflow Interrupt: On
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: Off
	// Compare B Match Interrupt: Off
	TCCR1A = (0 << COM1A1) | (0 << COM1A0) | (0 << COM1B1) | (0 << COM1B0) | (0 << WGM11) | (0 << WGM10);
	TCCR1B = (0 << ICNC1)  | (0 << ICES1)  | (0 << WGM13)  | (0 << WGM12)  | (0 << CS12)  | (1 << CS11) | (0 << CS10);
	TCNT1H = 0x00;
	TCNT1L = 0x01;
	ICR1H  = 0x00;
	ICR1L  = 0x00;
	OCR1AH = 0x00;
	OCR1AL = 0x00;
	OCR1BH = 0x00;
	OCR1BL = 0x00;

	// Timer/Counter 2 initialization
	// Clock source: System Clock
	// Clock value: Timer 2 Stopped
	// Mode: Normal top=FFh
	// OC2 output: Disconnected
	ASSR = 0x00;
	TCCR2 = 0x00;
	TCNT2 = 0x00;
	OCR2 = 0x00;

	// External Interrupt(s) initialization
	// INT0: Off
	// INT1: On
	// INT1 Mode: Low level
	GICR|=0x80;
	MCUCR=0x0C;
	GIFR=0x80;

	// Timer(s)/Counter(s) Interrupt(s) initialization
	TIMSK = 0x04;

	// Analog Comparator initialization
	// Analog Comparator: Off
	// Analog Comparator Input Capture by Timer/Counter 1: Off
	ACSR = 0x80;
	SFIOR = 0x00;
}
