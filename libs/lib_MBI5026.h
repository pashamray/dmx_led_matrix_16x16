/*
 *        File: lib_MBI5026.h
 *  Created on: 17.03.2012
 *      Author: Xakep.OK
 */

#ifndef LIB_MBI5026_H_
#define LIB_MBI5026_H_

#include <avr/io.h>

#define MBI5026_COUNT			(16)

#define MBI5026_PORT 			(PORTC)
#define MBI5026_DDR 			(DDRC)

#define MBI5026_DATA_PIN 		_BV(PC5)
#define MBI5026_CLK_PIN 		_BV(PC3)
#define MBI5026_LATCH_PIN		_BV(PC4)

#define MBI5026_LATCH_UP		{ MBI5026_PORT |= MBI5026_LATCH_PIN; }
#define MBI5026_LATCH_DOWN		{ MBI5026_PORT &= (uint8_t) ~MBI5026_LATCH_PIN; }
#define MBI5026_LATCH_PULSE		{ MBI5026_LATCH_UP; __asm__ __volatile__("nop"::); MBI5026_LATCH_DOWN; }

#define MBI5026_CLK_DOWN		{ MBI5026_PORT &= (uint8_t) ~MBI5026_CLK_PIN; }
#define MBI5026_CLK_UP			{ MBI5026_PORT |= MBI5026_CLK_PIN; }

extern void MBI5026_Init(void);
extern void MBI5026_Write(unsigned int data, unsigned char latch);

#endif /* LIB_MBI5026_H_ */
